# Info
This is an Occopus template for SSH tunneling. It is good for tunneling an obsolete protocol which has no encryption. I've expanded the solution with a cron job that watches the connection. And if it breaks down, than builds the tunnel up again.

## Occopus install
[Occopus setup guide](http://www.lpds.sztaki.hu/occo/user/html/setup.html)

## Build requirements
Tested in ubuntu 16.04, but it should work with other operating systems.

## Attributes
There are multiple variables that you can set to your own preference. These are in the infrastructure definition file infra-tunnel.yaml 
***
variables:
* username: ssh username
* serviceport: unencrypted service port
* localport: local port which the unencrypted service connects

## Start
```bash
$ occopus-import nodes/node_definitions.yaml
$ occopus-build infra-tunnel.yaml
```
